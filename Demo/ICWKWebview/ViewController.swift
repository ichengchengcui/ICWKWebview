//
//  ViewController.swift
//  ICWKWebview
//
//  Created by cc on 2018/7/21.
//  Copyright © 2018年 cc. All rights reserved.
//

import UIKit

class ViewController: ICWKWebVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "WKWebviewDemo"
        
        //注册方法
        registerWebHandle(["jsTest1"])
        loadHtml("https://www.baidu.com")
    }

    //    被调用方法
    @objc func jsTest1(_ dict: NSDictionary) {
        print("jsTest1--\(dict)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

