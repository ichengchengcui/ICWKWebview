//
//  ICWKWebVC.swift
//  ICWkWebDemo
//
//  Created by cuichengcheng on 2017/10/24.
//  Copyright © 2017年 cuichengcheng. All rights reserved.
//

import UIKit
import WebKit
class ICWKWebVC: UIViewController {
    public var webManager = ICWKLoadManager()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = .white
        
        webManager.delegate = self;
        view.addSubview(webManager.wkWebV)
        
        
    }
    
    public func loadHtml(_ urlStr: String){
        webManager.loadHtml(urlStr)
    }
    public func loadHtml(_ urlStr: String, _ parm: [String: Any]?){
        webManager.loadHtml(urlStr, parm)
    }
    
    func registerWebHandle(_ names: [String])  {
        webManager.registerWebHandle(names)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        webManager.wkWebV.frame = view.bounds
    
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ic_webback() {
        navigationController?.popViewController(animated: false)
    }
    
    deinit {
        
        print("释放\(self.classForCoder)")
    }
    
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ICWKWebVC: ICWKDelegate {
    func presentAler(_ webview: WKWebView, _ alert: UIAlertController) {
        present(alert, animated: true, completion: nil)
    }
    
    func setupWebTitle(_ webview: WKWebView, _ title: String) {
        self.title = title
    }
    func ic_goBack(_ webview: WKWebView) {
        if webview.canGoBack {
            webview.goBack()
        }else if self.presentedViewController != nil {
            dismiss(animated: true, completion: nil)
        }else if self.parent != nil {
            view.removeFromSuperview()
            removeFromParentViewController()
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
