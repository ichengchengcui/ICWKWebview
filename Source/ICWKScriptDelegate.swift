//
//  ICWKScriptDelegate.swift
//  ICWkWebDemo
//
//  Created by cuichengcheng on 2017/10/24.
//  Copyright © 2017年 cuichengcheng. All rights reserved.
//

import UIKit
import WebKit

protocol ICWKScriptProtocol: class {
    func performSel(_ name: String, parm:Any)
}

class ICWKScriptDelegate: NSObject, WKScriptMessageHandler {
    var names: [String] = []
    weak var delegate: ICWKScriptProtocol?
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        delegate?.performSel(message.name, parm: message.body)
        
    }
    deinit {
        print("释放\(self.classForCoder)")
    }
    
}
