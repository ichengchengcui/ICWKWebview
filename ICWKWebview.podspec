

Pod::Spec.new do |s|

  s.name         = "ICWKWebview"
  s.version      = "1.0.0"
  s.summary      = "ICWKWebview. base WKWebview"
  s.homepage     = "https://gitee.com/ichengchengcui/ICWKWebview"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "River" => "ichengchengcui@163.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://gitee.com/ichengchengcui/ICWKWebview.git", :tag => s.version.to_s }
  s.source_files  = "Source/*.{h,m,swift}"
  s.requires_arc = true
  s.framework  = "WebKit"
end
